﻿using TestApi.Shared;

namespace TestApi.Contracts {
    public interface IGeoLocationService : IService<GeoLocation>, IServiceInterfaceBase {
    }
}