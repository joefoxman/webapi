﻿namespace TestApi.Contracts {
    public interface IApiService : IServiceInterfaceBase {
        Task<T> GetData<T>(string url);
    }
}