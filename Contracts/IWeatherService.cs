﻿using TestApi.Shared;

namespace TestApi.Contracts {
    public interface IWeatherService : IService<Weather>, IServiceInterfaceBase {
    }
}