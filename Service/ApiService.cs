﻿using TestApi.Contracts;
using Flurl.Http;

namespace TestApi.Services {
    public class ApiService : IApiService {

        public async Task<T> GetData<T>(string url) {
            IFlurlResponse response;

            response = await url
                       .AllowAnyHttpStatus()
                       .GetAsync();

            if (response.StatusCode != 200) {
                return default;
            }
            var model = await response.GetJsonAsync<T>();
            return model;

        }
    }
}