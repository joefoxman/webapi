﻿namespace TestApi.Shared {
    public class CommonSettings {
        public string GetLocationServiceUrl { get; set; } = string.Empty;
        public string WeatherServiceUrl { get; set; } = string.Empty;
    }
}