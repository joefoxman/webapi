﻿namespace TestApi.Shared {
    public class DataWrapper {
        public Weather Weather { get; set; } = new Weather();
        public GeoLocation GeoLocation { get; set; } = new GeoLocation();
    }
}