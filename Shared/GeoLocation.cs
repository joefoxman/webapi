﻿namespace TestApi.Shared {
    public class GeoLocation {
        public string Ip { get; set; } = string.Empty;
        public Location Location { get; set; }  = new Location();
    }

    public class Location {
        public string Country { get; set; } = string.Empty;
        public string Region { get; set; } = string.Empty;
        public string TimeZone { get; set; } = string.Empty;
    }
}