﻿namespace TestApi.Shared {
    public class Weather {
        public string Latitude { get; set; } = string.Empty;
        public string Longitude { get; set; } = string.Empty;
        public string ResolvedAddress { get; set; } = string.Empty;
        public string Ttimezone { get; set; } = string.Empty;
        public string Tzoffset { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
    }
}