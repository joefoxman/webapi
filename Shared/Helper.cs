﻿namespace TestApi.Shared {
    public static class Helper {
        private readonly static int LowRange = 100;
        private readonly static int HighRange = 3000;

        public static int GetRandomDelayMilliseconds() {
            // random delay between 100 and 3000 milliseconds
            Random random = new();
            int delayMilliseconds = random.Next(LowRange, HighRange);
            return delayMilliseconds;
        }
    }
}