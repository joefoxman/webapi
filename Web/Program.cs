using TestApi.Contracts;
using TestApi.WebApi.Configuration;
using TestApi.WebApi.Middleware;

var builder = WebApplication.CreateBuilder(args);
string _clientAppUrl = "";

// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddLocalCors(_clientAppUrl);
builder.Services.AddServices<IServiceInterfaceBase>(builder.Services.AddScoped);
builder.Services.RegisterServices(builder.Configuration);
builder.Services.AddSpaStaticFiles(configuration =>
{
    configuration.RootPath = "ClientApp/dist";
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment()) {
    app.UseSwagger();
    app.UseSwaggerUI();
}

//app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();

var isDevelopment = System.Diagnostics.Debugger.IsAttached;
if (isDevelopment) {
    app.UseStaticFiles();
    app.UseSpaStaticFiles();
    app.UseRouting();
    app.UseAuthorization();
    var newPort = Web.Middleware.Util.TcpPortFinder.FindAvailablePort();
    app.UsePreflightRequestHandler($"http://localhost:{newPort}");
    app.UseEndpoints(endpoints => {
        endpoints.MapControllers();
    });
    app.UseVueDevelopmentServer(port: newPort);
}
else {
    app.UseEndpoints(endpoints =>
    {
        endpoints.MapControllers();
    });
    app.UseSpa(spa =>
    {
        spa.Options.SourcePath = "ClientApp";
    });
}

app.Run();
