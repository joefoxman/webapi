using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using TestApi.Contracts;
using TestApi.Shared;

namespace TestApi.WebApi.Controllers {
    [ApiController]
    [Route("[controller]")]
    public class TestController : ControllerBase {
        private readonly IWeatherService _weatherService;
        private readonly IGeoLocationService _geoLocationService;

        public TestController(
            IWeatherService weatherService,
            IGeoLocationService geoLocationService) {

            _weatherService = weatherService;
            _geoLocationService = geoLocationService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("GetLocalWeather")]
        public async Task<IActionResult> GetLocalWeather(string zipCode) {
            Stopwatch stopWatch = new();
            stopWatch.Start();

            var weatherData = await _weatherService.GetData(zipCode);

            stopWatch.Stop();
            Console.WriteLine("GetData Total Time Elapsed={0}", stopWatch.Elapsed);

            return Ok(weatherData);
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("GetGeoLocation")]
        public async Task<IActionResult> GetGeoLocation(string? ipAddress = null) {
            Stopwatch stopWatch = new();
            stopWatch.Start();

            var geoLocationData = await _geoLocationService.GetData(ipAddress);

            stopWatch.Stop();
            Console.WriteLine("GetData Total Time Elapsed={0}", stopWatch.Elapsed);

            return Ok(geoLocationData);
        }


        [HttpGet]
        [AllowAnonymous]
        [Route("GetData")]
        public async Task<IActionResult> GetData(string zipCode, string? ipAddress = null) {
            Stopwatch stopWatch = new();
            stopWatch.Start();

            var dataWrapper = new DataWrapper {
                Weather = await _weatherService.GetData(zipCode),
                GeoLocation = await _geoLocationService.GetData(ipAddress ?? string.Empty)
            };

            stopWatch.Stop();
            Console.WriteLine("GetData Total Time Elapsed={0}", stopWatch.Elapsed);

            return Ok(dataWrapper);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("GetDataParallel")]
        public async Task<IActionResult> GetDataParallel(string zipCode, string? ipAddress = null) {
            Stopwatch stopWatch = new();
            stopWatch.Start();

            var weatherTask = Task.Run(async () => await _weatherService.GetData(zipCode));
            var geoLocationTask = Task.Run(async () => await _geoLocationService.GetData(ipAddress ?? string.Empty));

            await Task.WhenAll(weatherTask, geoLocationTask);

            var dataWrapper = new DataWrapper {
                Weather = weatherTask.Result,
                GeoLocation = geoLocationTask.Result
            };

            stopWatch.Stop();
            Console.WriteLine("GetDataParallel Total Time Elapsed={0}", stopWatch.Elapsed);

            return Ok(dataWrapper);
        }
    }
}