module.exports = {
  pages: {
    index: {
      entry: 'src/main.ts'
    }
  },
  css: {
    extract: { ignoreOrder: true },
  },
  configureWebpack: {
    devtool: 'source-map',
    devServer: {
      port: 5248,
      proxy: 'http://localhost/',
      historyApiFallback: true
    },
    optimization: {
      splitChunks: {
        chunks: 'all',
        minSize: 100000
      }
    },
    performance: {
      hints: "warning", 
      maxEntrypointSize: 100000, // int (in bytes)
      maxAssetSize: 100000 // int (in bytes),
    }
  },
  chainWebpack: config => {
    config.performance
      .maxEntrypointSize(400000)
      .maxAssetSize(400000);
  },
  assetsDir: '',
  publicPath: process.env.NODE_ENV === 'production'
    ? '/'
    : '/',
  transpileDependencies: [
    'vuetify'
  ]
}
