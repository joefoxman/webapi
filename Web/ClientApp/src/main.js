"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var vue_1 = require("vue");
var router_1 = require("./router");
var vuetify_1 = require("./plugins/vuetify");
var store_1 = require("./store");
var signalR_1 = require("./signalR");
vue_1.default.config.productionTip = true;
vue_1.default.config.silent = true;
vue_1.default.use(signalR_1.default);
new vue_1.default({
    el: "#app-root",
    router: router_1.default,
    vuetify: vuetify_1.default,
    store: store_1.default,
    render: function (h) { return h(require("./components/app/app.vue").default); }
});
//# sourceMappingURL=main.js.map