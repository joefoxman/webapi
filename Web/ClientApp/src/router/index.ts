import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/home/home.vue'

Vue.use(Router)

const routes = [
  { path: "/", component: Home, name: 'home' },
]

const router = new Router({
  routes,
  mode: 'history',
  base: process.env.BASE_URL
})

export default router
