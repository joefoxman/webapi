import { __decorate } from "tslib";
import Vue from "vue";
import { Component, Prop } from "vue-property-decorator";
let AppComponent = class AppComponent extends Vue {
    constructor() {
        super(...arguments);
        this.disabled = false;
    }
};
__decorate([
    Prop({ default: false, type: [Boolean] })
], AppComponent.prototype, "disabled", void 0);
AppComponent = __decorate([
    Component
], AppComponent);
export default AppComponent;
//# sourceMappingURL=app.js.map