import Vue from "vue";
import { Component, Prop } from "vue-property-decorator";

@Component
export default class AppComponent extends Vue {
    @Prop({ default: false, type: [Boolean] })
    disabled = false;
}