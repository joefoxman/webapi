﻿export class Common {
  public static getRequestOptions(method: string, model: any) {
    let requestOptions = {};
    if (model !== null) {
      requestOptions = {
        method: method,
        headers: { "Content-Type": "application/json; charset=utf-8" },
        body: JSON.stringify(model),
      };
    }
    else {
      requestOptions = {
        method: method,
      };
    }
    return requestOptions;
  }
}
