import { SelectedFilterData } from "../../viewmodels/filters/filters.viewmodel";
import Filter from "../filter/filter.vue";
import Header from "../app/header.vue";
import Calendar from "../calendar/calendar.vue";
import Grid from "../grid/grid.vue";
import ReviewHistoryGrid from "../reviewhistory/reviewhistory.vue";
import Footer from "../app/footer.vue";
import Register from "../register/registerbor.vue";
import ReviewDateChange from "../review/information/reviewDateChange.vue";
import { ApiService } from "../apiService";
import { mapGetters } from "vuex";
export default {
    components: {
        "header-component": Header,
        "filter-component": Filter,
        "calendar-component": Calendar,
        "grid-component": Grid,
        "reviewhistory-component": ReviewHistoryGrid,
        "footer-component": Footer,
        "registerbor-component": Register,
        "review-date-change-component": ReviewDateChange,
    },
    props: {
        disabled: {
            type: Boolean,
            default: false
        },
        readonly: {
            type: Boolean,
            default: false
        },
    },
    data() {
        return {
            loading: false,
            selectedFilterData: new SelectedFilterData(),
            calendarEvents: [],
            scheduleGridData: [],
            reviewHistory: [],
            apiService: new ApiService(),
        };
    },
    computed: {
        ...mapGetters({
            data: 'currentData'
        }),
        canCreateReview() {
            return this.data.systemViewModel.canCreateReview;
        }
    },
    watch: {
        selectedFilterData: {
            immediate: true,
            async handler(newValue, oldValue) {
                this.getGridAndCalendarData(newValue);
            }
        }
    },
    mounted() {
        this.getGridAndCalendarData(this.selectedFilterData);
        this.$root.$on("onRefreshGridAll", () => {
            this.getGridData(this.selectedFilterData);
        });
    },
    methods: {
        async getGridAndCalendarData(selectedFilterData) {
            let url = "/api/Review/GetGridCalendarDataAndHistory/";
            let data = await this.apiService.fetchWithBody(url, 'POST', selectedFilterData);
            this.calendarEvents = data.calendarEvents;
            this.scheduleGridData = data.scheduleGridUis;
            this.reviewHistory = data.reviewHistory;
            this.$emit("update:data", this.calendarEvents);
            this.$emit("update:data", this.scheduleGridData);
            this.$emit("update:data", this.reviewHistory);
        },
        async getGridData(selectedFilterData) {
            let url = "/api/Review/GetGridData/";
            let data = await this.apiService.fetchWithBody(url, 'POST', selectedFilterData);
            this.scheduleGridData = data;
            this.$emit("update:data", this.scheduleGridData);
            this.getHistoryGrid(selectedFilterData);
        },
        async getHistoryGrid(selectedFilterData) {
            let url = "/api/Review/GetHistoryGrid/";
            let data = await this.apiService.fetchWithBody(url, 'POST', selectedFilterData);
            this.reviewHistory = data;
            this.$emit("update:data", this.reviewHistory);
        },
        register() {
            this.$root.$emit("showRegisterBor", true, "");
        },
        reset() {
            this.$refs.filter.reset();
            this.$refs.calendar.reset();
        }
    }
};
//# sourceMappingURL=home.js.map