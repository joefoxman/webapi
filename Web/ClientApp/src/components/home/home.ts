import { ApiService } from "../apiService";
export default {
  components: {
  },
  props: {
  },
  data() {
    return {
      apiService: new ApiService() as ApiService,
      localWeatherData: '' as String,
      geoLocationData: '' as String,
      getLocalWeatherUrl: 'Test/GetLocalWeather?zipCode=' as String,
      getGeoLocationUrl: 'Test/GetGeoLocation' as String,
      getDataUrl: 'Test/GetData?zipCode=' as String,
      getDataParallelUrl: 'Test/GetDataParallel?zipCode=' as String,
      zipCode: '' as String,
    }
  },
  computed:{
  },
  watch:{
  },
  mounted() {
  },
  methods: {
    onClear() {
      this.localWeatherData = null;
      this.geoLocationData = null;
    },
    async getLocalWeather(){
      let query = this.getLocalWeatherUrl + this.zipCode.toString();
      this.localWeatherData = await this.apiService.fetchGet(query);
    },
    async getGeoLocation(){
      this.geoLocationData = await this.apiService.fetchGet(this.getGeoLocationUrl);
    },
    async getData() {
      let query = this.getDataUrl + this.zipCode.toString();
      let wrapper = await this.apiService.fetchGet(query);
      this.localWeatherData = wrapper.weather;
      this.geoLocationData = wrapper.geoLocation;
    },
    async getDataParallel() {
      let query = this.getDataParallelUrl + this.zipCode.toString();
      let wrapper = await this.apiService.fetchGet(query);
      this.localWeatherData = wrapper.weather;
      this.geoLocationData = wrapper.geoLocation;
    }
  }
}
