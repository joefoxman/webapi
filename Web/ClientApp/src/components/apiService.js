"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiService = void 0;
var common_1 = require("./common");
var store_1 = require("../store");
var ApiService = /** @class */ (function () {
    function ApiService() {
    }
    ApiService.prototype.fetchGet = function (url) {
        var requestOptions = common_1.Common.getRequestOptions("GET", null);
        requestOptions.method = "GET";
        return this.fetchInternal(url, requestOptions);
    };
    ApiService.prototype.fetchWithBody = function (url, method, body, additionalHeaders) {
        var availableMethods = ["POST", "PUT", "DELETE"];
        if (availableMethods.indexOf(method) == -1) {
            throw new Error("Method not supported");
        }
        var requestOptions = common_1.Common.getRequestOptions("POST", body);
        requestOptions.method = method;
        requestOptions.body = JSON.stringify(body);
        return this.fetchInternal(url, requestOptions);
    };
    ApiService.prototype.fetchInternal = function (url, requestOptions) {
        return __awaiter(this, void 0, void 0, function () {
            var response, statusText, responseObject, parsedResponse, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        // start the spinner
                        store_1.default.dispatch('incremantLoading');
                        return [4 /*yield*/, fetch((process.env.VUE_APP_API_BASE_URL || '') + url, requestOptions)];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.text()];
                    case 2:
                        statusText = _a.sent();
                        responseObject = {
                            status: response.status,
                            text: statusText
                        };
                        parsedResponse = void 0;
                        try {
                            parsedResponse = JSON.parse(responseObject.text);
                        }
                        catch (error) {
                            // notification to the user
                            // use the swal
                            parsedResponse = {};
                        }
                        // stop the spinner
                        store_1.default.dispatch('decrementLoading');
                        return [2 /*return*/, parsedResponse];
                    case 3:
                        error_1 = _a.sent();
                        // stop the spinner
                        store_1.default.dispatch('decrementLoading');
                        // notification to the user
                        // use the swal
                        throw error_1;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    return ApiService;
}());
exports.ApiService = ApiService;
//# sourceMappingURL=apiService.js.map