﻿using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.Extensions.Options;
using System.Reflection;
using TestApi.Shared;
using MsConfiguration = Microsoft.Extensions.Configuration.IConfiguration;

namespace TestApi.WebApi.Configuration {
    public static class ServiceCollectionExtensions {
        public static IServiceCollection AddServices<T>(this IServiceCollection services, Func<Type, Type, IServiceCollection> registerFunc) {
            Assembly[] assesmblies = { Assembly.Load("TestApi.Services") };
            var types = assesmblies.SelectMany(x => x.GetExportedTypes()
                .Where(y => y.IsClass && !y.IsAbstract && !y.IsGenericType && !y.IsNested && typeof(T).IsAssignableFrom(y))).ToList();

            foreach (var type in types) {
                var interfaces = type.GetInterfaces();
                foreach (var @interface in interfaces) {
                    registerFunc(@interface, type);
                }
            }
            return services;
        }

        public static IServiceCollection AddLocalCors(this IServiceCollection services, string clientAppUrl) {
            if (services == null) {
                throw new ArgumentNullException(nameof(services));
            }
            var isLocal = System.Diagnostics.Debugger.IsAttached;
            if (isLocal) {
                var corsBuilder = new CorsPolicyBuilder();
                corsBuilder.AllowAnyHeader();
                corsBuilder.AllowAnyMethod();
                corsBuilder.AllowAnyOrigin();

                services.AddCors(options => {
                    options.AddPolicy("CorsPolicy", corsBuilder.Build());
                });

                var corsBuilderSignalR = new CorsPolicyBuilder();
                corsBuilderSignalR.AllowAnyHeader();
                corsBuilderSignalR.AllowAnyMethod();
                corsBuilderSignalR.WithOrigins(clientAppUrl);
                corsBuilderSignalR.AllowCredentials();

                services.AddCors(options => {
                    options.AddPolicy("CorsPolicySignalR", corsBuilderSignalR.Build());
                });
            }
            return services;
        }

        public static IServiceCollection RegisterServices(this IServiceCollection services, MsConfiguration configuration) {
            services.Configure<CommonSettings>(options => configuration.GetSection("Common").Bind(options));
            // Explicitly register the settings object by delegating to the IOptions object
            services.AddSingleton(resolver => resolver.GetRequiredService<IOptions<CommonSettings>>().Value);
            return services;
        }
    }
}
