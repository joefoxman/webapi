﻿using System.Net;

namespace TestApi.WebApi.Middleware
{
    public class PreflightRequestMiddleware
    {
        private readonly RequestDelegate Next;
        private readonly string _clientAppUrl;

        public PreflightRequestMiddleware(RequestDelegate next, string clientAppUrl)
        {
            Next = next;
            _clientAppUrl = clientAppUrl;
        }
        public Task Invoke(HttpContext context)
        {
            return BeginInvoke(context);
        }
        private Task BeginInvoke(HttpContext context)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                context.Response.Headers.Add("Access-Control-Allow-Origin", new[] { _clientAppUrl });
                context.Response.Headers.Add("Access-Control-Allow-Credentials", new[] { "true" });
                context.Response.Headers.Add("Access-Control-Allow-Headers", new[] { "x-signalr-user-agent, Content-Type, Authorization, X-Requested-With" });
                context.Response.Headers.Add("Access-Control-Allow-Methods", new[] { "GET", "POST, DELETE, PATCH, PUT" });
                if (context.Request.Method == HttpMethod.Options.Method)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.OK;
                    return context.Response.WriteAsync("OK");
                }
            }
            return Next.Invoke(context);
        }
    }

    public static class PreflightRequestExtensions
    {
        public static IApplicationBuilder UsePreflightRequestHandler(this IApplicationBuilder builder, string clientAppUrl)
        {
            return builder.UseMiddleware<PreflightRequestMiddleware>(clientAppUrl);
        }
    }
}
