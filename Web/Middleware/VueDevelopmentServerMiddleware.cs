﻿using Microsoft.AspNetCore.Hosting.Server.Features;
using System.Text.RegularExpressions;
using Web.Middleware.Util;

namespace TestApi.WebApi.Middleware
{
    internal static class VueDevelopmentServerMiddleware
    {
        private const string PatternMatchSucess = "App running at";
        private const string PatternMatchError = "ERROR  Failed to compile";
        private const string LogCategoryName = "Microsoft.AspNetCore.SpaServices";
        private static readonly TimeSpan RegexMatchTimeout = TimeSpan.FromSeconds(5); // This is a development-time only feature, so a very long timeout is fine
        private static NpmScriptRunner _npmScriptRunner;

        public static int Attach(
            IApplicationBuilder appBuilder,
            string sourcePath,
            string npmScriptName,
            int? port = null)
        {
            if (string.IsNullOrEmpty(sourcePath))
            {
                throw new ArgumentException("Cannot be null or empty", nameof(sourcePath));
            }

            if (string.IsNullOrEmpty(npmScriptName))
            {
                throw new ArgumentException("Cannot be null or empty", nameof(npmScriptName));
            }

            var done = false;
            var logger = LoggerFinder.GetOrCreateLogger(appBuilder, LogCategoryName);
            if (port == null)
            {
                port = TcpPortFinder.FindAvailablePort();
            }

            // Start Vue devevelopment server
            var taskReturn = StartVueDevServerAsync(appBuilder, sourcePath, npmScriptName, port.Value, logger);
            var targetTask = taskReturn.ContinueWith(
                task =>
                    new TaskReturn
                    {
                        Port = task.Result.Port,
                        Message = task.Result.Message,
                        Uri = new UriBuilder("http", "localhost", task.Result.Port).Uri
                    }
                );
            // Add middleware to the pipeline that waits for the Vue development server to start
            appBuilder.Use(async (context, next) =>
            {
                if (!done)
                {
                    // On each request, we create a separate startup task with its own timeout. That way, even if
                    // the first request times out, subsequent requests could still work.
                    var timeout = TimeSpan.FromSeconds(120); // this is a dev only middleware, long timeout is probably fine
                    await targetTask.WithTimeout(timeout,
                        $"The vue development server did not start listening for requests " +
                        $"within the timeout period of {timeout.Seconds} seconds. " +
                        $"Check the log output for error information.");

                    await next();
                }
            });

            if (done)
            {
                return port.Value;
            }
            // Redirect all requests for root towards the Vue development server, 
            // using the resolved targetUriTask
            appBuilder.Use(async (context, next) =>
            {
                if (context.Request.Path == "/")
                {
                    var task = await targetTask;
                    if (targetTask.Result.Message.Equals(PatternMatchSucess, StringComparison.OrdinalIgnoreCase))
                    {
                        context.Response.Redirect(task.Uri.AbsoluteUri);
                    }
                    else
                    {
                        throw new CompileErrorException(
                            $"Vue Failed to compile " +
                            $"Please refer to the log for error details " +
                            _npmScriptRunner.LogData());
                    }
                    done = true;
                }
                else
                {
                    await next();
                }
            });
            return port.Value;
        }

        private static async Task<TaskReturn> StartVueDevServerAsync(
            IApplicationBuilder appBuilder,
            string sourcePath,
            string npmScriptName,
            int portNumber,
            ILogger logger)
        {

            logger.LogInformation($"Starting Vue dev server on port {portNumber}...");

            // Inject address of .NET app as the ASPNET_URL env variable and read it in vue.config.js from process.env
            // as opposed to hardcoding https://localhost:5001 as the address of the dotnet web server
            // NOTE: When running with IISExpress this will be empty, so you will need to hardcode the URL in IISExpress as a fallbacl
            // when ASPNET_URL is empty
            var addresses = appBuilder.ServerFeatures.Get<IServerAddressesFeature>().Addresses;
            var envVars = new Dictionary<string, string> {
                { "ASPNET_URL", addresses.Count > 0 ? addresses.First() : "" },
            };
            _npmScriptRunner = new NpmScriptRunner(
                sourcePath, npmScriptName, $"--port {portNumber} --host localhost", envVars);
            _npmScriptRunner.AttachToLogger(logger);

            Match matchFound;
            using (var stdErrReader = new EventedStreamStringReader(_npmScriptRunner.StdErr))
            {
                try
                {
                    // Although the Vue dev server may eventually tell us the URL it's listening on,
                    // it doesn't do so until it's finished compiling, and even then only if there were
                    // no compiler warnings. So instead of waiting for that, consider it ready as soon
                    // as it starts listening for requests.
                    matchFound = await _npmScriptRunner.StdOut.WaitForMatches(
                        new List<Regex> { new Regex(PatternMatchSucess, RegexOptions.IgnoreCase, RegexMatchTimeout),
                                          new Regex(PatternMatchError, RegexOptions.IgnoreCase, RegexMatchTimeout)});
                }
                catch (EndOfStreamException ex)
                {
                    throw new InvalidOperationException(
                        $"The NPM script '{npmScriptName}' exited without indicating that the " +
                        $"vue development server was listening for requests. The error output was: " +
                        $"{stdErrReader.ReadAsString()}", ex);
                }
            }

            return new TaskReturn { Port = portNumber, Message = matchFound.Value };
        }
    }
}